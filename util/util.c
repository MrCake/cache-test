#include<stdio.h>
#include<stdlib.h>
#include"util.h"



float * zero_arr(int size)
{
    return (float *) calloc(size, 4);
}

float *rand_arr(int size, float max_num)
{
    float *gen = (float *) malloc(size * 4);
    for(int i=0; i<size; i++)
        gen[i] = ((float)rand()/(float)(RAND_MAX)) * max_num;

    return gen;
}