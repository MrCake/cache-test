all: test1


test1: test1.cpp obj/util.o obj/example.o
	g++ -Wall -Wextra -pedantic -o test1 test1.cpp obj/util.o obj/example.o


obj/example.o: x64/example.s
	nasm -f elf64 ./x64/example.s -o ./obj/example.o

obj/util.o: util/util.c
	g++ -Wall -c  ./util/util.c -o ./obj/util.o