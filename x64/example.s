section .data

section .text
global add

add:
    mov   rax, rdi   ; argument 1
    add   rax, rsi   ; argument 2
    ret
