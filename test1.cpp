#include"./util/util.h"
#include <iostream>
#include <time.h>
#include <valgrind/callgrind.h>

#define DSPL_MAX 120
#define REPETITIONS 1
#define ITERATIONS 1  // 100.000.000

// intel laptop
// #define CACHE_LINE_SIZE 64      //bytes
// #define CACHE_L1_SIZE 32768 
// #define CACHE_L2_SIZE 131072    
// #define CACHE_L3_SIZE 786432


#define CACHE_LINE_SIZE 64
#define CACHE_L1_SIZE 1024 * 512 
#define CACHE_L2_SIZE 1048576 * 4
#define CACHE_L3_SIZE 1048576  * 16
#define SETS_L1 8
#define SETS_L2 8
#define SETS_L3 16

extern "C" int add(int a, int b);

int main() 
{
    int el_cacheline = (CACHE_LINE_SIZE / sizeof(float));
    int el_cacheL3 = (CACHE_L3_SIZE / sizeof(float));

    long arr_size = el_cacheL3 * 2;
    float max_num = 255.0;

    float *arr = zero_arr( arr_size );
    float *dest = rand_arr( arr_size, max_num);

    // float *times = zero_arr((REPETITIONS+1) * (DSPL_MAX+1));

    // clock_t start, end;
    
    // start = clock();
    // long index;
    int c;

    CALLGRIND_START_INSTRUMENTATION;
    for(long index = 0 ; index <  el_cacheL3 ; index += el_cacheline)
    {
        // index = it % el_cacheL3;
        // dest[index] = arr[index];
        c += arr[index];
    }
    CALLGRIND_STOP_INSTRUMENTATION;
    // end = clock();
    //    times[n + (dspl*REPETITIONS)] = ((float) (end - start)) / CLOCKS_PER_SEC; 
    
    // std::cout << add(1,2) << std::endl;
    free(arr);
    free(dest);

    return 0;
}
